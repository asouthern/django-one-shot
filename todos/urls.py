from django.urls import path
from todos.views import (
    show_todo_list,
    details_of_todo,
    create_todo,
    edit_todo,
    todo_list_delete,
)


urlpatterns = [
    path("", show_todo_list, name="todo_list_list"),
    path("<int:id>/", details_of_todo, name="todo_list_detail"),
    path("create/", create_todo, name="create_todo"),
    path("<int:id>/edit/", edit_todo, name="edit_todo"),
    path("<int:id>/delete/", todo_list_delete, name="todo_list_delete"),
]
