from django.shortcuts import render, get_object_or_404, redirect
from todos.models import TodoList
from todos.forms import TodoForm


# Create your views here.
def show_todo_list(request):
    todo_list = TodoList.objects.all()
    context = {
        "todo_list_object": todo_list,
    }

    return render(request, "todos/list.html", context)


# this view shows the specific details of a todo list
def details_of_todo(request, id):
    todo_item = get_object_or_404(TodoList, id=id)
    context = {"todo_item": todo_item}

    return render(request, "todos/details.html", context)


# this view alows a user to create a todo list
def create_todo(request):
    if request.method == "POST":
        form = TodoForm(request.POST)
        if form.is_valid():
            form.save()
            return redirect("todo_list_list")

    else:
        form = TodoForm()

    context = {"form": form}

    return render(request, "todos/create.html", context)


# this view allows the user to update a todo list
def edit_todo(request, id):
    todo_list = TodoList.objects.get(id=id)

    if request.method == "POST":
        form = TodoForm(request.POST, instance=todo_list)
        if form.is_valid():
            todo_list = form.save()
            return redirect("todo_list_list")

    else:
        form = TodoForm(instance=todo_list)

    context = {
        "form": form,
    }

    return render(request, "todos/edit.html", context)


# this view allows the user to delete a list
def todo_list_delete(request, id):
    todo_list = TodoList.objects.get(id=id)
    if request.method == "POST":
        todo_list.delete()
        return redirect("todo_list_list")
    return render(request, "todos/delete.html")
